<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//contact routing

Route::get('/contact', 'ContactController@showContactForm');
Route::get('/contact/confirm', 'ContactController@showContactConfirm');
Route::get('/contact/success', 'ContactController@contactSubmitted');

//admin contact
Route::get('/admin/contact', 'AdminContactController@contact');
Route::get('/admin/contact/{id}', 'AdminContactController@contactId');
//Route::get('/admin/contact', 'AdminContactController@contact');
