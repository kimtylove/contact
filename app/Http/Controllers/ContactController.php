<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function showContactForm() {
        return view('pages.contact');
    }

    public function showContactConfirm() {
        return view('pages.contactConfirm');
    }

    public function contactSubmitted() {
        return view('pages.contactSuccess');

    }
}
