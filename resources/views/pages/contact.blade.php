@extends('layouts.master')

@section('title', 'Contact | Page')

@push('style')

    <style>
        .float-right {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    </style>

@endpush

@section('content')

    @include('partials.NavBar', [ 'title' => 'Contact' ])

    <form>
        <div class="row">
            <div class="col-sm-12">
                <label> Name* </label>
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-6">
                <label>Email*</label>
                <input type="email" class="form-control">
            </div>
            <div class="col-sm-6">
                <label>PhoneNumber</label>
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-12">
                <label> Subject </label>
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-12">
                <label> Message </label>
                <textarea class="form-control" aria-label="With textarea" rows="10"></textarea>
            </div>
        </div>
        <div class="float-right">
            <button class="btn btn-secondary" type="button">Next ></button>
        </div>
        <div class="clearfix"></div>
    </form>



@endsection





