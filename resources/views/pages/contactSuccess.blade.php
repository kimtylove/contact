@extends('layouts.master')
@section('title', 'Contact | Submit')

@push('style')
    <style>

        .text-center, .text-center button {
            margin-top: 100px;
            margin-bottom: 100px;
        }
        .text-center strong {
            color: grey;
            font-size: 20px;
        }
        .text-center p {
            font-size: 14px;
        }
    </style>
@endpush
@section('content')
    @include('partials.NavBar', ['title' => 'contact Submited'])
    <div class="text-center">
        <strong>you message has been submitted to our system!</strong>
        <p>Our team will feedback to you soon, Thank you</p>
        <button class="btn btn-secondary">GO BACK HOMEPAGE</button>
    </div>
@endsection
