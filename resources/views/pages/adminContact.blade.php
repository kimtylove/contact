@extends('layouts.master')
@section('title', 'Contact | List')

@push('style')
    <style>
        table {
            margin-top: 15px;
            border: 1px solid darkgrey;
        }

        .pagination .page-item .page-link {
            color: darkgray;
        }

        .float-left > p {
            font-size: 14px;
        }

        .clearfix.upper-right-menu .float-right {
            margin-bottom: 7px;
        }

        .left-menu .dropdown {
            display: inline-block;
        }

        .right-menu  {
            display: inline-block;
        }

        .right-menu .navbar {
            padding: 0;

        }

        .modal-footer {
            justify-content: space-between;
        }


    </style>
@endpush
@section('content')

    @include('partials.NavBar', ['title' => 'Contact List'])
    <div class="clearfix upper-right-menu">
        <div class="float-right">
            <button type="button" class="btn btn-light" data-toggle="modal" data-target=".bd-example-modal-xl">
                Bulk Email
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
            </button>
            @include('partials.modal', ['titleModal' => 'BULK EMAILS'])
        </div>
    </div>
    <div class="d-flex justify-content-between">
        <div class="left-menu">
            <div class="dropdown">
                <a class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Bulk Actions
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </div>
            <button class="btn btn-outline-secondary my-2 my-sm-0" type="button">APPLY</button>
            <div class="dropdown">
                <a class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Bulk Actions
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </div>
            <button class="btn btn-outline-secondary my-2 my-sm-0" type="button">FILTER</button>
        </div>
        <div class="right-menu">
            <nav class="navbar navbar-light">
                <form class="form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Enter Keyword ..." aria-label="Search">
                    <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Search</button>
                </form>
            </nav>
        </div>

    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#No</th>
                <th scope="col">Name</th>
                <th scope="col">Subject</th>
                <th scope="col">Email</th>
                <th scope="col">Status</th>
                <th scope="col">Date</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @for($i = 0; $i < 10; ++$i)
                <tr>
                    <th scope="row">#1</th>
                    <td>mr.constructor</td>
                    <td>support</td>
                    <td>constructor@dev.com</td>
                    <td>pending</td>
                    <td>02/02/2020</td>
                    <td>Edit|</td>
                </tr>
            @endfor
        </tbody>
    </table>
    <div class="clearfix row">
        <div class="col-sm-6">
            <div class="float-left">
                <p>Total Items: 50</p>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="float-right">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

@endsection
