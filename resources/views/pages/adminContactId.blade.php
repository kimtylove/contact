@extends('layouts.master')
@section('title', 'Contact | View')

@push('style')
    <style>
        .multiple-btn {
            padding-top: 0;
            padding-bottom: 15px;
        }
        .modal-footer {
            justify-content: space-between;
        }
    </style>
@endpush

@section('content')

    @include('partials.NavBar', ['title' => 'contact View'])

    <div class="multiple-btn float-right clearfix">
        <button class="btn btn-light">Pending</button>
        <button class="btn btn-light">Inprogress</button>
        <button class="btn btn-light">Completed</button>
    </div>
    <x-resuableContact></x-resuableContact>

    <div class="clearfix">
        <button class="btn btn-secondary float-left">BACK</button>
        <button type="button" class="btn btn-secondary float-right" data-toggle="modal" data-target=".bd-example-modal-xl">
            Bulk Email
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
        </button>
        @include('partials.modal', ['titleModal' => 'EMAIL'])
    </div>

@endsection
