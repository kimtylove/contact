@extends('layouts.master')

@section('title', 'Contact | Confirm')

@push('style')
    <style>
        tr {
            overflow-x: unset;
            overflow-y: visible;

        }
        .table th, .table td {
            padding-left: 25px;
            padding-right:25px ;

        }
        .table tr:last-child th, .table tr:last-child td {
            padding-bottom: 35px;
        }
        table {
            margin-top: 40px;
        }
    </style>
@endpush

@section('content')

    @include('partials.NavBar', ['title' => 'contact Confirmation'])

    <x-resuableContact></x-resuableContact>

    @include('partials.reusableButton', ['btnLeft' => 'BACK', 'btnRight' => 'SUBMIT'])

@endsection
