@push('style')
    <style>
        .content-title {
            margin-top: 10px;
            margin-bottom: 20px;
        }
    </style>
@endpush

<div class="content-title">
    <div class="display-4">
        <h1 > {{  $title }} </h1>
        <hr/>
    </div>
</div>
