<div class="clearfix">
    <button class="btn btn-secondary float-left">{{ $btnLeft }}</button>
    <button class="btn btn-secondary float-right">{{ $btnRight }}</button>
</div>
